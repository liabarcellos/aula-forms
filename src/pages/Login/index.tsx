import React from "react";
import { Text, TextInput, TouchableOpacity, View } from "react-native";

import { useForm, Controller } from "react-hook-form";

interface FormData {
  email: string;
  password: string;
}

function Login() {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<FormData>({ mode: "onTouched" });

  const onSubmit = (data: FormData) => {
    console.log(data);
  };

  return (
    <View>
      <Controller
        control={control}
        name="email"
        render={({ field: { onBlur, onChange, value } }) => (
          <TextInput
            onBlur={onBlur}
            onChangeText={(value) => onChange(value)}
            value={value}
            placeholder="Insira seu e-mail"
          />
        )}
        defaultValue=""
        rules={{
          required: "O e-mail é obrigatório",
          pattern: {
            value: /^\S+@\S+$/i,
            message: "E-mail inválido",
          },
        }}
      />
      {errors.email && (
        <Text style={{ color: "red" }}>{errors.email.message}</Text>
      )}
      <Controller
        control={control}
        name="password"
        render={({ field: { onBlur, onChange, value } }) => (
          <TextInput
            secureTextEntry={true}
            onBlur={onBlur}
            onChangeText={(value) => onChange(value)}
            value={value}
            placeholder="Insira sua senha"
          />
        )}
        defaultValue=""
        rules={{
          required: "A senha é obrigatória",
        }}
      />
      {errors.password && (
        <Text style={{ color: "red" }}>{errors.password.message}</Text>
      )}
      <TouchableOpacity onPress={handleSubmit(onSubmit)}>
        <Text>Entrar</Text>
      </TouchableOpacity>
    </View>
  );
}

export default Login;
