import React from "react";

import { Text, TextInput, TouchableOpacity, View } from "react-native";

import { useForm, Controller } from "react-hook-form";

import { TextInputMask } from "react-native-masked-text";

interface RegisterData {
  name: string;
  cpf: string;
}

function Register() {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<RegisterData>({ mode: "onTouched" });

  const onSubmit = (data: RegisterData) => {
    console.log(data);
  };

  return (
    <View>
      <Controller
        control={control}
        name="name"
        render={({ field: { onBlur, onChange, value } }) => (
          <TextInput
            onBlur={onBlur}
            onChangeText={(value) => onChange(value)}
            value={value}
            placeholder="Insira seu nome"
          />
        )}
        defaultValue=""
        rules={{
          required: "O nome é obrigatório",
        }}
      />
      {errors.name && (
        <Text style={{ color: "red" }}>{errors.name.message}</Text>
      )}
      <Controller
        control={control}
        name="cpf"
        render={({ field: { onBlur, onChange, value } }) => (
          <TextInputMask
            type="cpf"
            onBlur={onBlur}
            onChangeText={(value: string) => onChange(value)}
            value={value}
            placeholder="Insira seu cpf"
          />
        )}
        defaultValue=""
        rules={{
          required: "O cpf é obrigatório",
        }}
      />
      {errors.cpf && <Text style={{ color: "red" }}>{errors.cpf.message}</Text>}
    </View>
  );
}

export default Register;
