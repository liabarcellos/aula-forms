# aula-forms

Aula de Formulários, Máscaras e Validação para o TT de 2022.1

## Como rodar

1. Clonar o projeto
2. Rodar `cd aula-forms`
3. Rodar `yarn install`
4. Rodar `expo start --web`

